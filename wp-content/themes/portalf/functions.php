<?php

add_action( 'after_setup_theme', 'browncoats_theme_setup' );

function browncoats_theme_setup() {
	global $content_width;

	/* Set the $content_width for things such as video embeds. */
	if ( !isset( $content_width ) )
		$content_width = 600;

	/* Add theme support for automatic feed links. */	
	add_theme_support( 'automatic-feed-links' );

	/* Add theme support for post thumbnails (featured images). */
	add_theme_support( 'post-thumbnails' );

	/* Add theme support for custom backgrounds. */
	add_custom_background();
	add_custom_background();

	/* Add your nav menus function to the 'init' action hook. */
	add_action( 'init', 'browncoats_register_menus' );

	/* Add your sidebars function to the 'widgets_init' action hook. */
	add_action( 'widgets_init', 'browncoats_register_sidebars' );

	/* Load JavaScript files on the 'wp_enqueue_scripts' action hook. */
	add_action( 'wp_enqueue_scripts', 'browncoats_load_scripts' );
}

function browncoats_register_menus() {
	/* Register nav menus using register_nav_menu() or register_nav_menus() here. */
}

function browncoats_register_sidebars() {
	/* Register dynamic sidebars using register_sidebar() here. */
}

function browncoats_load_scripts() {
	/* Enqueue custom Javascript here using wp_enqueue_script(). */

	/* Load the comment reply JavaScript. */
	if ( is_singular() && get_option( 'thread_comments' ) && comments_open() )
		wp_enqueue_script( 'comment-reply' );
}

// Menu Principal
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Woocommerce
function my_theme_wrapper_start() {
  echo '<section id="commerce-main">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

// Remove admin bar from front-end
function remove_admin_bar(){
  return false;
}
add_filter( "show_admin_bar" , "remove_admin_bar");

function the_breadcrumb() {
	if (!is_home()) {
		echo '<a class="bc-item bc-item-home" href="'. get_option('home'). '">Home</a>';
		if (is_category() || is_single()) {
			the_category('title_li=');
			if (is_single()) {
				echo "<span class='bc-item'>";
				the_title();
				echo "</span>";
			}
		} elseif (is_page()) {
			echo "<span class='bc-item'>";
			the_title();
			echo "</span>";
		}
	}
}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {
	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

function redirect_to_checkout() {
    return WC()->cart->get_checkout_url();
}
add_filter ('add_to_cart_redirect', 'redirect_to_checkout');

?>