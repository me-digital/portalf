// Gulpfile
var gulp = 		require('gulp'),
	sass = 		require('gulp-ruby-sass'),
	imgmin = 	require('gulp-imagemin'),
    jshint =    require('gulp-jshint'),
	uglify = 	require('gulp-uglify'),
	concat = 	require('gulp-concat'),
	plumber = 	require('gulp-plumber'),
    del =       require('del'),
	rsync = 	require('rsyncwrapper').rsync;

// Paths
var paths =  {
    // JS files
    js: [
        // Vendors
        'dev/js/vendor/jquery.js', 'dev/js/vendor/slick.js',
        // Custom
        'dev/js/partials/*.js','dev/js/main.js'
    ],

    // Scss Files
    styles: 'dev/styles/main.scss',

    // Images
    images: 'dev/images/**/*.{jpg,png,gif}'
};

// TASKS
// ---------------------------------

// Compile Sass
gulp.task('styles', function(){
	return sass(paths.styles, {
			sourcemap: false,
			style: 'compressed'
		})
        .pipe(plumber())
		.on('error', function(err){
			console.error('[ERRO | styles]:', err.message);
		})
		.pipe(gulp.dest('css/')) // source dest
});

// Concat
gulp.task('jsmin', function(){
    return gulp.src(paths.js)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(plumber())
        .on('error', function(err){
            console.error('[ERRO | jsmin]:', err.message);
        })
        .pipe(gulp.dest('js/')) // source dest
});

// Images
gulp.task('imagemin', function(){
    return gulp.src(paths.images)
        .pipe(imgmin())
        .on('error', function(err){
            console.error('[ERRO | imgmin]:', err.message);
        })
        .pipe(gulp.dest('images/'))
});

// Clean
gulp.task('clean', function(cb) {
  del(['css', 'images', 'js'], cb);
});

// Watch
gulp.task('watch', function() {
    gulp.watch('dev/styles/**/*scss', ['styles']);
    gulp.watch(paths.js, ['jsmin']);
    gulp.watch(paths.images, ['imagemin']);
});

gulp.task('default', ['clean', 'styles', 'jsmin', 'imagemin']);

