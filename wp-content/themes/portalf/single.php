<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php 
						get_template_part( 'template-parts/content', 'single' ); 
						the_content();
					?>

				<?php endwhile; // End of the loop. ?>		
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
