<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>
	
	<!-- Main -->
	<main id="main" class="site-main" role="main">
		<div class="container">
			<div id="breadcrumb">
				<?php the_breadcrumb(); ?>	
			</div>
			<!-- Page Title -->
			<h2 class="page-name"><?php echo get_the_title(); ?></h2>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php the_content(); ?>

				<?php
					/* Comentario Padrão
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					*/
				?>

			<?php endwhile; // End of the loop. ?>
		</div>
	</main><!-- #main -->
	

<?php get_sidebar(); ?>
<?php get_footer(); ?>
