	<?php include('inc/newsletter.php') ?>
    <!-- Footer -->
	<footer id="footer">
        <div class="container">
            <div class="col sitemap">
                <img src="<?php echo get_template_directory_uri(); ?>/images/logo-footer.png" alt="Logo Portal F">
                <?php
                    // Menu Header 
                    wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); 
                ?>
            </div>
            
            <!-- Trabalhe Conosco -->
            <div class="col trabalhe-conosco">
                <div class="box">
                    <div class="content">
                        <span class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icons/trabalhe-conosco.png" alt="Icone Trabalhe Conosco" />
                        </span>
                        <h2 class="description">
                            Quer trabalhar<br>conosco?
                            <a href="#" title="Trabalhe Conosco">Clique aqui</a>
                        </h2>
                    </div>
                </div>
            </div>

            <!-- Ligue Agora -->
            <div class="col ligue-agora">
                <p>
                    Ligue agora
                    <span>4004-4004</span>
                </p>
                <img src="<?php echo get_template_directory_uri();?>/images/map-footer.png" alt="Mapa de Localização" />
            </div>

            <!-- Central de Atendimento -->
            <div class="col central-atendimento">
                <h2 class="title">Central de Atendimento</h2>

                <div class="contatos">
                    <ul>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                        <li>Salvador (BA) 71 3444-6030</li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Copyright -->
        <div id="copyright">
            <div class="container">
                <div class="col">
                    <p>&copy; Copyright <?php echo date('Y'); ?> Portal F</p>
                </div>
                <div class="col powered">
                    <p>Produzido por <a href="http://www.medigital.com.br" target="_blank">Me Digital</a></p>
                </div>
                <div class="col social">
                <ul>
                    <li><a href="#" class="icon icon-twitter"></a></li>
                    <li><a href="#" class="icon icon-facebook"></a></li>
                    <li><a href="http://estacio.br" class="logo-estacio"></a></li>
                </ul>
                </div>    
            </div>
        </div>
	</footer>
	<!-- END | Footer -->

    <!-- Overlay -->
	<div class="theme-overlay"></div>
    
	<!-- Scripts -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
	<?php wp_footer(); ?>
</body>
</html>
