<?php 
/**
* Template Name: Frontpage
**/
 ?>

<?php // Header
    get_header(); 
?>
	<!-- Main -->
    <main role="main">
	    <div class="container">
			<div id="content" class="site-content" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			</div>
		</div>
			
    </main>
    <!-- END | Main -->

<?php // Footer
    get_footer(); 
?>
