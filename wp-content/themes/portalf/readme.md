# MEWP

Wordpress blank theme for our projects.

## Get Started
1- Clone Project in yoursite/wp-content/themes/here
```
$ git clone git@bitbucket.org:brunodsgn/mewp-blank.git
```

2- Install gulp globally
```
$ npm install -g gulp
```

3- Install all dependendies of project
```
$ npm install
```
