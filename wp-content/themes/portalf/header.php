<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title>
        <?php 
            global $page , $paged ;
            wp_title('|', true, 'right');
            bloginfo('name');
        ?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" sizes="32x32">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    
    <!-- Header -->
    <header id="header">
        <div class="container">
            <!-- Logo -->
            <h1 id="logo">
                <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Logo">
                </a>
            </h1>
            
            <!-- Content Right -->
            <div id="content-header">
                <!-- Social Media Links -->
                <div class="row social-media">
                    <ul>
                        <li><a href="" target="_blank" class="icon icon-facebook">Facebook</a></li>
                        <li><a href="" target="_blank" class="icon icon-twitter">Twitter</a></li>
                    </ul>
                </div>
                
                <!-- Search and Feature Partner -->
                <div class="row row-search">
                    <div class="content">
                        <div id="search">
                            <?php get_search_form(); ?>
                        </div>
                        <div class="feature-partner">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/estacio.png" alt="Estacio" />
                        </div>    
                    </div>
                </div>

                <!-- Nav Aux -->
                <div class="row aux-nav">
                    <div class="nav-content">
                        <ul class="nav-items">
                            <li class="tel">Ligue Agora <span>4004-4004</span></li>
                            <li class="atendimento-online">Atendimento <span>Online</span></li>
                            <li class="matricula">Faça já sua <span>Matrícula</span></li>
                            <li class="area-aluno">Área do <span>Aluno</span></li>
                        </ul>
                    </div>
                </div>

                <!-- Navigation -->
                <div class="row">
                    <nav role="navigation">
                        <?php
                            // Menu Header 
                            wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); 
                        ?>
                    </nav>      
                </div>
                
            </div>
        </div>
    </header>
    
